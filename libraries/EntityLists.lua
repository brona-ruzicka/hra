local entityList = {}
local parent = nil
EntityList = {}

function EntityList.getList()
	return entityList
end

function EntityList.init(parent_id)
	parent = parent_id
	for key, value in pairs(entityList) do
		go.set_parent(value, parent, true)
	end
end

function EntityList.add(object_id)
	if parent then
		go.set_parent(object_id, parent, true)
	end
	table.insert(entityList, object_id)
end

function EntityList.remove(object_id)
	for key, value in pairs(entityList) do
		if value == object_id then
			table.remove(entityList, key)
			return
		end
	end
end

local disabledDragList = {}
DisabledDragList = {}

function DisabledDragList.isInList(object_id)
	for key, value in pairs(disabledDragList) do
		if value == object_id then
			return true
		end
	end
	return false
end

function DisabledDragList.add(object_id)
	table.insert(disabledDragList, object_id)
end

function DisabledDragList.remove(object_id)
	for key, value in pairs(disabledDragList) do
		if value == object_id then
			table.remove(disabledDragList, key)
			return
		end
	end
end

function DisabledDragList.removeAll()
	disabledDragList = {}
end