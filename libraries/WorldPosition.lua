local world = nil
local background = nil

WorldPosition = {}

function WorldPosition.init(world_id, background_id)
	world = world_id
	background = background_id
end

function WorldPosition.getPosition(screenPos)
	if go.get_scale(background).x ~= 1 then return nil end
	local coords = ((
		(-(go.get_position(world) + go.get_position(background)) + screenPos)
		/ SharedValues.tileSize)
		* (1 + 3 * ((SharedValues.zoomLevel + 1) % 2)))
	coords.z = 0
	return coords
end

--function WorldPosition.getScreenPosition(worldPos) end --TODO