local originalSize = vmath.vector3(tonumber(sys.get_config("display.width")), tonumber(sys.get_config("display.height")), 1)
local size = vmath.vector3(originalSize)

WindowSize = {}

local function callback(self, event, data)
	if event == window.WINDOW_EVENT_RESIZED then
		print("Window resized: ", data.width, data.height)
		WindowSize.save(data.width, data.height)
	end
end

local function calculate_size(width, height) 
	local currentRatio = width / height
	local originalRatio = originalSize.x / originalSize.y
	if currentRatio < originalRatio then
		return vmath.vector3(originalSize.x, originalSize.x * (1 / currentRatio), 1)
	elseif currentRatio > originalRatio then
		return vmath.vector3(originalSize.y * currentRatio, originalSize.y, 1)
	else 
		return vmath.vector3(originalSize.x, originalSize.y, 1)
	end
end

function WindowSize.init(window) 
	window.set_listener(callback)
end

function WindowSize.save(x, y)
	size = calculate_size(x, y)
end

function WindowSize.get()
	return size
end

function WindowSize.getDefault() 
	return originalSize
end