-- https://stackoverflow.com/questions/33425333/lua-perlin-noise-generation-getting-bars-rather-than-squares
-- original code by Ken Perlin: http://mrl.nyu.edu/~perlin/noise/

require("libraries.Rand")

local function BitAND(a,b)--Bitwise and
	local p,c=1,0
	while a>0 and b>0 do
		local ra,rb=a%2,b%2
		if ra+rb>1 then c=c+p end
		a,b,p=(a-ra)/2,(b-rb)/2,p*2
	end
	return c
end

local function fade( t )
	return t * t * t * (t * (t * 6 - 15) + 10)
end

local function lerp( t, a, b )
	return a + t * (b - a)
end

local function grad( hash, x, y, z )
	if not hash then print("hash is nil") end
	local h = BitAND(hash, 15)
	local u = h < 8 and x or y
	local v = h < 4 and y or ((h == 12 or h == 14) and x or z)
	return ((h and 1) == 0 and u or -u) + ((h and 2) == 0 and v or -v)
end

PerlinNoise = {
	size = 256,
	gx = {},
	gy = {},
	randMax = 256,
}

function PerlinNoise.init(seed)
	local r = Rand(seed)
	PerlinNoise.permutation = {}
	for i = 1, PerlinNoise.size do
		PerlinNoise.permutation[i] = r:random(1, 256)
	end
end

function PerlinNoise.p(n)
	return PerlinNoise.permutation[n % PerlinNoise.size + 1]
end

function PerlinNoise.noise( x, y, z )
local X = BitAND(math.floor(x), 255) + 1
local Y = BitAND(math.floor(y), 255) + 1
local Z = BitAND(math.floor(z), 255) + 1

x = x - math.floor(x)
y = y - math.floor(y)
z = z - math.floor(z)
local u = fade(x)
local v = fade(y)
local w = fade(z)
local A  = PerlinNoise.p(X) + Y 
local AA = PerlinNoise.p(A) + Z
local AB = PerlinNoise.p(A + 1) + Z
local B  = PerlinNoise.p(X + 1) + Y
local BA = PerlinNoise.p(B) + Z
local BB = PerlinNoise.p(B + 1) + Z

return lerp(w, lerp(v, lerp(u, grad(PerlinNoise.p(AA  ), x  , y  , z  ),
grad(PerlinNoise.p(BA  ), x-1, y  , z  )),
lerp(u, grad(PerlinNoise.p(AB  ), x  , y-1, z  ),
grad(PerlinNoise.p(BB  ), x-1, y-1, z  ))),
lerp(v, lerp(u, grad(PerlinNoise.p(AA+1), x  , y  , z-1),
grad(PerlinNoise.p(BA+1), x-1, y  , z-1)),
lerp(u, grad(PerlinNoise.p(AB+1), x  , y-1, z-1),
grad(PerlinNoise.p(BB+1), x-1, y-1, z-1))))
end