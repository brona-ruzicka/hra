SharedValues = {

	tileMapSize = 16,
	tileSize = 32,
	overFlowMargin = 128,
	zoomLevel = 0,
	zoomDuration = 0.5,

}