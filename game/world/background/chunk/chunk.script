require("libraries.SharedValues")
require("libraries.PerlinNoise")

go.property("chunkX", 0)
go.property("chunkY", 0)

local function terrainType(x, y)

	local perlin = 0
	if SharedValues.zoomLevel == 1 then
		perlin = PerlinNoise.noise( (x + 3) / 32 + 500 , (y + 3) / 32 + 500 , 0.3)
	else
		perlin = PerlinNoise.noise(       x /  8 + 500 ,       y /  8 + 500 , 0.3)
	end
		
	local tile
	if perlin < -0.09 then
		tile = 2
	elseif perlin < 0.15 then
		tile = 0
	elseif perlin < 0.3 then
		tile = 1
	else
		tile = 3
	end
	return tile
end

local function setTile(tileX, tileY, lb, rb, lt, rt) --left bottom +(0, 0), right bottom +(1, 0), left top +(1, 0), right top +(1, 1)
	local terrainTypes = {}
	
	terrainTypes[lb] = 1
	terrainTypes[rb] = (terrainTypes[rb] or 0) + 2
	terrainTypes[lt] = (terrainTypes[lt] or 0) + 4
	terrainTypes[rt] = (terrainTypes[rt] or 0) + 8

	if terrainTypes[lb] == 15 then 
		terrainTypes[lb] = 15 + math.random(4)
	end

	for i = 0, 3 do
		local value = terrainTypes[i] or 0
		if value == 6 or value == 9 then
			terrainTypes[i] = value + 16
			break
		end
	end
	
	tilemap.set_tile("#tilemap",    "plains", tileX, tileY, (terrainTypes[0] or 0) +  1)
	tilemap.set_tile("#tilemap",    "forest", tileX, tileY, (terrainTypes[1] or 0) + 33)
	tilemap.set_tile("#tilemap",     "water", tileX, tileY, (terrainTypes[2] or 0) + 65)
	tilemap.set_tile("#tilemap", "mountains", tileX, tileY, (terrainTypes[3] or 0) + 97)
end

function generate(chunkX, chunkY)
	return function()
		local vertices = {}
		for x = 1, SharedValues.tileMapSize + 1 do
			for y = 1, SharedValues.tileMapSize + 1 do
				if not vertices[x] then vertices[x] = {} end
				vertices[x][y] = terrainType(x + chunkX * SharedValues.tileMapSize, y + chunkY * SharedValues.tileMapSize)
			end
		end
		for x = 1, SharedValues.tileMapSize do
			for y = 1, SharedValues.tileMapSize do
				setTile(x, y, vertices[x][y], vertices[x + 1][y], vertices[x][y + 1], vertices[x + 1][y + 1])
			end
		end
	end
end

function init(self)
	generate(self.chunkX, self.chunkY)()
end

function final(self)
end

function on_message(self, message_id, message)
	if message_id == hash("chunk_update") then
		if self.chunkX == message.x and self.chunkY == message.y and not message.immediately then return end
		self.chunkX = message.x
		self.chunkY = message.y
		if message.immediately then
			generate(self.chunkX, self.chunkY)()
		else
			timer.delay(0, false, generate(self.chunkX, self.chunkY))
		end
	elseif message_id == hash("animate_alpha") then
		go.animate("#tilemap", "tint.w", go.PLAYBACK_ONCE_FORWARD, message.alpha, go.EASING_INOUTSINE, 0.15)
	end
end

